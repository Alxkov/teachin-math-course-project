"""
Используя метод союзных матриц, найти матрицу, обратную к
$$
\begin{pmatrix}
-2 & 3 & -2\\
3 & -5 & 4\\
-2 & 3 & -3
\end{pmatrix}
$$
"""
from functools import reduce

CORRECT_ANSWER_STR = "-3 -3 -2\n-1 -2 -2\n1 0 -1"
CORRECT_ANSWER_MX = [[-3, -3, -2], [-1, -2, -2], [1, 0, -1]]
INCORRECT_ANSWER_WO_DETERMINANT = [[3, 3, 2], [1, 2, 2], [-1, 0, 1]]
INCORRECT_ANSWER_UNTRANSPOSED = [[-3, -1, 1], [-3, -2, 0], [-2, -2, -1]]
INCORRECT_ANSWER_BOTH_MISTAKES = [[3, 1, -1], [3, 2, 0], [2, 2, 1]]


class Feedback:
    SYNTAX_ERROR = 'Ошибка при вводе ответа'
    CORRECT = 'Верно!'
    INCORRECT_NONINVERSIBLE = 'Неверно. Эта матрица невырождена и у неё существует обратная. Попробуйте её найти'
    INCORRECT_WO_DETERMINANT = 'Не забывайте, что союзную матрицу необходимо поделить на определелитель исходной'
    INCORRECT_UNTRANSPOSED = 'Не забывайте, что матрицу алгебраических дополнений необходимо транспонировать'
    INCORRECT_BOTH_MISTAKES = 'Не забывайте, что матрицу алгебраических дополнений необхримо транспонировать ' \
                              'и поделить на определитель исходной'
    INCORRECT_WRONG_DIM = 'Размерность обратной матрицы должна совпадать с исходной'
    INCORRECT_ONE_EL = 'Вы ошиблись лишь в одном из элементов обратной матрицы, попробуйте найти ошибку'
    INCORRECT_TWO_EL = 'Вы ошиблись в расчётах двух элементах обратной матрицы, попробуйте найти ошибки'
    INCORRECT_OTHER = 'Неверно, попробуйте ещё раз'


##############################


def solve():
    return CORRECT_ANSWER_STR


def parse(reply):
    if reply == '0':
        return '0'
    try:
        matrix = [[float(j) for j in i] for i in [i.split(' ') for i in reply.split('\n')]]
    except ValueError:
        return None
    return matrix


def count_answer_reply_diff(answer, reply):
    return reduce(lambda x, y: x + y, ([reduce(lambda x, y: x + y, [int(j[0] != j[1]) for j in i]) for i in map(lambda ans_row, rep_row: list(map(lambda x, y: [x, y], ans_row, rep_row)), answer, reply)]))


def check(reply):
    answer = parse(reply)
    if answer is '0':
        return 0, Feedback.INCORRECT_NONINVERSIBLE
    if answer is None:
        return 0, Feedback.SYNTAX_ERROR
    if answer == CORRECT_ANSWER_MX:
        return 1, Feedback.CORRECT
    if answer == INCORRECT_ANSWER_WO_DETERMINANT:
        return 0, Feedback.INCORRECT_WO_DETERMINANT
    if answer == INCORRECT_ANSWER_UNTRANSPOSED:
        return 0, Feedback.INCORRECT_UNTRANSPOSED
    if answer == INCORRECT_ANSWER_BOTH_MISTAKES:
        return 0, Feedback.INCORRECT_BOTH_MISTAKES
    if len(answer) != 3:
        return 0, Feedback.INCORRECT_WRONG_DIM
    for row in answer:
        if len(row) != 3:
            return 0, Feedback.INCORRECT_WRONG_DIM
    diff = count_answer_reply_diff(answer, CORRECT_ANSWER_MX)
    if diff == 1:
        return 0, Feedback.INCORRECT_ONE_EL
    if diff == 2:
        return 0, Feedback.INCORRECT_TWO_EL
    return 0, Feedback.INCORRECT_OTHER
