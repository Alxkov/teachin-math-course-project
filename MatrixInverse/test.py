from task import check, Feedback

TEST_CASES = [
    # reply, score, feedback
    ("-3 -3 -2\n-1 -2 -2\n1 0 -1", 1, Feedback.CORRECT),
    ('-3 -1 1\n-3 -2 0\n-2 -2 -1', 0, Feedback.INCORRECT_UNTRANSPOSED),
    ('3 3 2\n1 2 2\n-1 0 1', 0, Feedback.INCORRECT_WO_DETERMINANT),
    ('3 1 -1\n3 2 0\n2 2 1', 0, Feedback.INCORRECT_BOTH_MISTAKES),
    ('[-3 -3 -2\n-1 -2 -2\n1 0 -1]', 0, Feedback.SYNTAX_ERROR),
    ('123 123 32\n23 231 23\n123 123 23', 0, Feedback.INCORRECT_OTHER),
    ('3 2 1\n2 1 213', 0, Feedback.INCORRECT_WRONG_DIM),
    ('3 2\n2 1', 0, Feedback.INCORRECT_WRONG_DIM),
    ('0', 0, Feedback.INCORRECT_NONINVERSIBLE),
    ('abcc, cdaasd', 0, Feedback.SYNTAX_ERROR),
    ("-3 -3 -2\n-1 -2 -2\n1 2 -1", 0, Feedback.INCORRECT_ONE_EL),
    ("-3 -3 -1\n-1 7 -2\n1 0 -1", 0, Feedback.INCORRECT_TWO_EL)
]

is_passed = True
for test_num, (reply, test_score, test_feedback) in enumerate(TEST_CASES, start=1):
    score, feedback = check(reply)
    if score != test_score or feedback != test_feedback:
        print(f'Test {test_num} FAILED: {score} != {test_score}')
        is_passed = False

if is_passed:
    print('All tests PASSED')
